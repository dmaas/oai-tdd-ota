#!/bin/bash

# # to get to image from UBUNTU_1804_STD
sudo apt update
sudo apt upgrade
sudo apt-get install linux-lowlatency-hwe-18.04
# have to reboot before uninstalling currently active kernel

# remove other kernels+headers
sudo apt remove --purge -y linux-image-4.15.0-7*
sudo apt remove --purge -y linux-image-unsigned-4.15.0-7*
sudo apt remove --purge -y linux-headers-4.15.0-7*
sudo update-grub2
sudo apt install -y cpufrequtils i7z linux-tools-common linux-tools-lowlatency linux-tools-5.3.0-28-lowlatency

# Get the emulab repo
while ! wget -qO - http://repos.emulab.net/emulab.key | sudo apt-key add -
do
    echo Failed to get emulab key, retrying
done

while ! sudo add-apt-repository -y http://repos.emulab.net/powder/ubuntu/
do
    echo Failed to get johnsond ppa, retrying
done

while ! sudo apt-get update
do
    echo Failed to update, retrying
done

# Install gnuradio and uhd images
while ! sudo DEBIAN_FRONTEND=noninteractive apt-get install -y gnuradio libuhd-dev
do
    echo Failed to get gnuradio, retrying
done

while ! sudo "/usr/lib/uhd/utils/uhd_images_downloader.py"
do
    echo Failed to download uhd images, retrying
done

# Get the OAI RAN src
cd /local || exit
while ! git clone https://gitlab.eurecom.fr/oai/openairinterface5g.git
do
    echo Failed to clone openairinterface5g, retrying
done

sudo sysctl -w net.core.rmem_max=33554432
sudo sysctl -w net.core.wmem_max=33554432

# these will not persist after reboot, so they must be called in startup script
for ((i=0;i<$(nproc);i++)); do sudo cpufreq-set -c $i -r -g performance; done
sudo cpupower idle-set -D 2

cd /local/openairinterface5g/ || exit
git checkout v1.2.0
