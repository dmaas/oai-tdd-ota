#!/usr/bin/env python

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.urn as URN
import geni.rspec.emulab.pnext as PN


tourDescription = """

# EXPERIMENTAL PROFILE TO TEST SDR UE

"""

tourInstructions = """
After booting is complete,

1 First you need to recompile eNB in node `enb1`:

cd /opt/oai/openairinterface5g

bash

source oaienv

cd cmake_targets/

sudo ./build_oai -c -C --eNB -w USRP


2 recompile UE in node `rue1`:

cd /opt/oai/openairinterface5g

bash

source oaienv

cd ./cmake_targets/

sudo ./build_oai -c -C --UE -w USRP # add -x for softscope

cd ./tools/

sudo ./init_nas_s1 UE

3 add ueinfo in node 'epc'

mysql -u root -p # password : linux

``` bash
mysql> use oai_db
mysql> INSERT INTO pdn (`id`, `apn`, `pdn_type`, `pdn_ipv4`, `pdn_ipv6`, `aggregate_ambr_ul`, `aggregate_ambr_dl`, `pgw_id`, `users_imsi`, `qci`, `priority_level`,`pre_emp_cap`,`pre_emp_vul`, `LIPA-Permissions`) VALUES ('3',  'oai.ipv4','IPV4', '0.0.0.0', '0:0:0:0:0:0:0:0', '50000000', '100000000', '2',  '998981234560308', '9', '15', 'DISABLED', 'ENABLED', 'LIPA-ONLY');
mysql> INSERT INTO users (`imsi`, `msisdn`, `imei`, `imei_sv`, `ms_ps_status`, `rau_tau_timer`, `ue_ambr_ul`, `ue_ambr_dl`, `access_restriction`, `mme_cap`, `mmeidentity_idmmeidentity`, `key`, `RFSP-Index`, `urrp_mme`, `sqn`, `rand`, `OPc`) VALUES ('998981234560308',  '33638060308', NULL, NULL, 'PURGED', '120', '50000000', '100000000', '47', '0000000000', '1', 0x00112233445566778899aabbccddeeff, '1', '0', 0, 0x00000000000000000000000000000000, 0x0ED47545168EAFE2C39C075829A7B61F);
```

4 run oai enb and EPC in node `enb1`

sudo /local/repository/bin/start_oai.pl

5 run tdd ue in node `rue1`:

cd  /opt/oai/openairinterface5g/targets/bin

sudo ./lte-softmodem.Rel14  -U -T -C 2350000000 --ue-txgain 70 --ue-rxgain 90 --ue-scan-carrier -r 25 # add -d for softscope


"""


#
# Globals
#
class GLOBALS(object):
    OAI_UE_IMG = "urn:publicid:IDN+emulab.net+image+PowderAlexOffice:oaidev1804lowlatency"
    OAI_ENB_IMG = "urn:publicid:IDN+emulab.net+image+PowderAlexOffice:oaidev1804lowlatency"
    OAI_EPC_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    UE_SETUP_CMD = "/local/repository/bin/setup_ue.sh"
    ENB_SETUP_CMD = "/local/repository/bin/setup_enb.sh"
    EPC_SETUP_CMD = "/local/repository/bin/setup_epc.sh"


pc = portal.Context()
request = pc.makeRequestRSpec()
s1_lan_1 = request.Link("s1-lan-1")
s1_lan_2 = request.Link("s1-lan-2")
x310_link = request.Link("x310-link")
n310_link = request.Link("n310-link")

enb1 = request.RawPC("enb1")
enb1.component_id = "pc01-meb"
enb1.disk_image = GLOBALS.OAI_ENB_IMG

enb2 = request.RawPC("enb2")
enb2.component_id = "sm01"
enb2.disk_image = GLOBALS.OAI_ENB_IMG

enb_x310_radio = request.RawPC("enb-x310-radio")
enb_x310_radio.component_id = "alex-pa"
x310_link.addNode(enb_x310_radio)

ue_n310_radio = request.RawPC("ue-n310-radio")
ue_n310_radio.component_id = "alex-pc"
n310_link.addNode(ue_n310_radio)

ue_b210 = request.RawPC("ue-b210")
ue_b210.component_id = "sm02"
ue_b210.disk_image = GLOBALS.OAI_UE_IMG

ue_n310 = request.RawPC("ue-n310")
ue_n310.component_id = "pc02-meb"
ue_n310.disk_image = GLOBALS.OAI_ENB_IMG

usrp_if1 = enb1.addInterface("usrp_if1")
usrp_if1.addAddress(rspec.IPv4Address("192.168.40.1", "255.255.255.0"))
x310_link.addInterface(usrp_if1)

usrp_if2 = ue_n310.addInterface("usrp_if2")
usrp_if2.addAddress(rspec.IPv4Address("192.168.40.1", "255.255.255.0"))
n310_link.addInterface(usrp_if2)

# Add a link connecting the NUC eNB and the OAI EPC node.
# epclink.addNode(enb1)
s1_lan_1.addNode(enb1)
s1_lan_2.addNode(enb2)

# Add OAI EPC (HSS, MME, SPGW) node.
epc_1 = request.RawPC("epc-1")
epc_1.disk_image = GLOBALS.OAI_EPC_IMG

epc_2 = request.RawPC("epc-2")
epc_2.disk_image = GLOBALS.OAI_EPC_IMG
# epc.addService(rspec.Execute(shell="sh", command=GLOBALS.EPC_SETUP_CMD))

s1_lan_1.addNode(epc_1)
s1_lan_1.link_multiplexing = True
s1_lan_1.vlan_tagging = True
s1_lan_1.best_effort = True

s1_lan_2.addNode(epc_2)
s1_lan_2.link_multiplexing = True
s1_lan_2.vlan_tagging = True
s1_lan_2.best_effort = True

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)
pc.printRequestRSpec(request)
